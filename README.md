# Description #
A library for getting axis and button data from gamepads/joysticks
in Linux.  A parallel thread keeps track of the current state of
all buttons and sticks.  Because of this, it's OK for reading stick positions in a loop but not for precisely detecting events like button presses.

# Testing #
Works on a Beaglebone Black running Debian 3.8.  Works great for a Sony Playstation 2 Dual Shock controller plugged in through a Playstation/Nintendo 64-to-USB adapter.

# Based on #
https://gist.github.com/rdb/886466.

# Usage #

```
#!python

import js_linux
j = js_linux.joystick()
while True:
  print j.state

# <Ctrl-C> to break loop.

j.close() # You may need to agitate the gamepad here...
exit()
```

```
#!python

import js_linux, time
j = js_linux.joystick()
print list(j.state)[1]['Left stick, down']
time.sleep(1)
print list(j.state)[1]['Left stick, down']
time.sleep(1)
print list(j.state)[1]['Left stick, down']
j.close() # You may need to agitate the gamepad here...
exit()
```