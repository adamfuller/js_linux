from __future__ import print_function
from builtins import bytes
from builtins import object
import threading
import time
import multiprocessing as mp
import os, struct, array
from fcntl import ioctl

"""
Description
-----------
A library for getting axis and button data from gamepads/joysticks
in Linux.  A parallel thread keeps track of the current state of
all buttons and sticks.  Because of this, it's OK for reading stick
positions in a loop but not for precisely detecting events like
button presses.

Based on
--------
https://gist.github.com/rdb/8864666.


Usage
-----
import js_linux
j = js_linux.joystick()
while True:
  print(j.state)

"""

class joystick(object):

	def __init__(self, debug=False, fn='/dev/input/js0',
			model='Dualshock2 via Boom PS/N64 adaptor', normalize=True, dt=0.001):

		"""
		Instantiate a member of this class to open a Linux joystick
		device e.g. /dev/input/js0 and listen for events.

		Starts a "listening loop" in a parallel thread, which is used to update the .state object.

		Released by rdb under the Unlicense (unlicense.org)
		Based on information from:
		https://www.kernel.org/doc/Documentation/input/joystick-api.txt
		Got from: https://gist.github.com/rdb/8864666
		"""

		assert model in ['Dualshock2 via Boom PS/N64 adaptor', 'Dualshock 3 Sixaxis', 'generic']

		self._debug = debug
		self._fn = fn
		self._model = model
		self._normalize=normalize
		self.dt = dt

		if self._debug:

			# Iterate over the joystick devices.
			print('Available devices:')

			for fn in os.listdir('/dev/input'):
				if fn.startswith('js'):
					print('  /dev/input/{}'.format(fn))

		# We'll store the states here.
		self._axis_states = {}
		self._button_states = {}

		if self._model=='Dualshock2 via Boom PS/N64 adaptor':

			"""
			These constants were determined experimentally with a
			Sony SCPH-110 analog ("DualShock") controller using a
			"BOOM"-brand N64/playstation-to-USB adaptor.
			"""

			self._axis_names = {
			    0x00 : 'Left stick, right',
			    0x01 : 'Left stick, down',
			    0x02 : 'Right stick, right',
			    0x05 : 'Right stick, down',
			}

			self._button_names = {
			    0x120 : 'Triangle',
			    0x121 : 'Circle',
			    0x122 : 'Ex',
			    0x123 : 'Square',
			    0x124 : 'L2',
			    0x125 : 'R2',
			    0x126 : 'L1',
			    0x127 : 'R1',
			    0x128 : 'Select',
			    0x129 : 'L3',
			    0x12a : 'R3',
			    0x12b : 'Start',
			    0x12c : 'D-pad up',
			    0x12d : 'D-pad right',
			    0x12e : 'D-pad down',
			    0x12f : 'D-pad left',
			}
		if self._model=='Dualshock 3 Sixaxis':
			"""
			These constants were determined experimentally with a
			Sony CECHZC2E wireless analog ("DUALSHOCK 3 SIXAXIS")
			controller, connected by USB.
			"""

			self._axis_names = {
			    0x00 : 'Left stick, right',
			    0x01 : 'Left stick, down',
			    0x02 : 'L2, squeezed',
			    0x03 : 'Right stick, right',
			    0x04 : 'Right stick, down',
			    0x05 : 'R2, squeezed',
			}

			self._button_names = {
			    0x133 : 'Triangle',
			    0x131 : 'Circle',
			    0x130 : 'Ex',
			    0x134 : 'Square',
			    0x138 : 'L2',
			    0x139 : 'R2',
			    0x136 : 'L1',
			    0x137 : 'R1',
			    0x13a : 'Select',
			    0x13d : 'L3',
			    0x13e : 'R3',
			    0x13b : 'Start',
			    0x220 : 'D-pad up',
			    0x223 : 'D-pad right',
			    0x221 : 'D-pad down',
			    0x222 : 'D-pad left',
			    0x13c : 'PS button',
			}
		if self._model=='generic':

			# Non-DualShock.
			# These constants were borrowed from linux/input.h
			self._axis_names = {
			    0x00 : 'x',
			    0x01 : 'y',
			    0x02 : 'z',
			    0x03 : 'rx',
			    0x04 : 'ry',
			    0x05 : 'rz',
			    0x06 : 'trottle',
			    0x07 : 'rudder',
			    0x08 : 'wheel',
			    0x09 : 'gas',
			    0x0a : 'brake',
			    0x10 : 'hat0x',
			    0x11 : 'hat0y',
			    0x12 : 'hat1x',
			    0x13 : 'hat1y',
			    0x14 : 'hat2x',
			    0x15 : 'hat2y',
			    0x16 : 'hat3x',
			    0x17 : 'hat3y',
			    0x18 : 'pressure',
			    0x19 : 'distance',
			    0x1a : 'tilt_x',
			    0x1b : 'tilt_y',
			    0x1c : 'tool_width',
			    0x20 : 'volume',
			    0x28 : 'misc',
			}

			self._button_names = {
			    0x120 : 'trigger',
			    0x121 : 'thumb',
			    0x122 : 'thumb2',
			    0x123 : 'top',
			    0x124 : 'top2',
			    0x125 : 'pinkie',
			    0x126 : 'base',
			    0x127 : 'base2',
			    0x128 : 'base3',
			    0x129 : 'base4',
			    0x12a : 'base5',
			    0x12b : 'base6',
			    0x12f : 'dead',
			    0x130 : 'a',
			    0x131 : 'b',
			    0x132 : 'c',
			    0x133 : 'x',
			    0x134 : 'y',
			    0x135 : 'z',
			    0x136 : 'tl',
			    0x137 : 'tr',
			    0x138 : 'tl2',
			    0x139 : 'tr2',
			    0x13a : 'select',
			    0x13b : 'start',
			    0x13c : 'mode',
			    0x13d : 'thumbl',
			    0x13e : 'thumbr',

			    0x220 : 'dpad_up',
			    0x221 : 'dpad_down',
			    0x222 : 'dpad_left',
			    0x223 : 'dpad_right',

			    # XBox 360 controller uses these codes.
			    0x2c0 : 'dpad_left',
			    0x2c1 : 'dpad_right',
			    0x2c2 : 'dpad_up',
			    0x2c3 : 'dpad_down',
			}

		self._axis_map = []
		self._button_map = []

		# Open the joystick device.
		if self._debug: print('Opening {}...'.format(self._fn))
		self._jsdev = open(self._fn, 'rb')

		# Get the device name.
		#buf = bytearray(63)
		#buf = array.array('B', ['\0'] * 64)
		buf = bytes(64)
		ioctl(self._jsdev, 0x80006a13 + (0x10000 * len(buf)), buf) # JSIOCGNAME(len)
		js_name = buf.decode()
		if self._debug: print('Device name: {}'.format(js_name))

		# Get number of axes and buttons.
		buf = array.array('B', [0])
		ioctl(self._jsdev, 0x80016a11, buf) # JSIOCGAXES
		num_axes = buf[0]

		buf = array.array('B', [0])
		ioctl(self._jsdev, 0x80016a12, buf) # JSIOCGBUTTONS
		num_buttons = buf[0]

		# Get the axis map.
		buf = array.array('B', [0] * 0x40)
		ioctl(self._jsdev, 0x80406a32, buf) # JSIOCGAXMAP

		for axis in buf[:num_axes]:
		    axis_name = self._axis_names.get(axis, 'unknown(0x%02x)' % axis)
		    self._axis_map.append(axis_name)
		    self._axis_states[axis_name] = 0.0

		# Get the button map.
		buf = array.array('H', [0] * 200)
		ioctl(self._jsdev, 0x80406a34, buf) # JSIOCGBTNMAP

		for btn in buf[:num_buttons]:
		    btn_name = self._button_names.get(btn, 'unknown(0x%03x)' % btn)
		    self._button_map.append(btn_name)
		    self._button_states[btn_name] = 0

		if self._debug:

			print('{} axes found: {}'.format(num_axes, ', '.join(self._axis_map)))
			print('{} buttons found: {}'.format(num_buttons, ', '.join(self._button_map)))


		"""
		Enter an infinite loop and update a persistent state object.
		"""

		man = mp.Manager()

		self.state = man.list()

		self._keepgoing = True

		def f(statev):

			while self._keepgoing:

				r = self._read()

				if r:

					statev[:] = list(r)

				time.sleep(dt)

		self._loopt = threading.Thread(target=f, args=(self.state,))

		self._loopt.start()

	def close(self):

		"""
		This will tidy up a joystick object.  You must do something
		(move stick or press button) after calling this method to allow
		this method to complete, because the read() call in the loopt
		process blocks.
		"""

		self._keepgoing = False

		self._loopt.join()

		self._jsdev.close()

	def _read(self):

		# This call to read() blocks until something on the joystick
		# changes, hence the multiprocessing-updated-state-variable
		# strategy.
		evbuf = self._jsdev.read(8)

		if evbuf:
			time, value, type, number = struct.unpack('IhBB', evbuf)

			if (type & 0x80):
				if self._debug: print("(initial)", end=' ')

			elif type & 0x01:
				button = self._button_map[number]
				if button:
					self._button_states[button] = value
				if self._debug:
					if value:
						print("{} pressed".format(button))
					else:
						print("{} released".format(button))

			elif type & 0x02:
				axis = self._axis_map[number]
				if axis:
					if self._normalize:
						fvalue = value / 32767.0
				else:
					fvalue = value
				self._axis_states[axis] = fvalue
				if self._debug: print("{}: {:.3f}".format(axis, fvalue))

			else:
				if self._debug: print('.', end='')
			
			return self._button_states, self._axis_states
		else:
			return "--empty--"

if __name__ == '__main__':
	model='Dualshock 3 Sixaxis'
	print("Testing assuming {}, edit code to test different joystick.".format(model))
	j = joystick(model=model, debug=True)
